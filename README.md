# Easy to send HTTP/HTTPS requests.
[![Crates.io](https://img.shields.io/crates/v/talap.svg)](https://crates.io/crates/talap)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/talap)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/talap/-/raw/master/LICENSE)


## Example

```rust
use talap::Request;
#[tokio::main]
async fn main() {
    use std::io::Write;
    let response = Request::get_from_url("https://www.apple.com/")
        .await
        .unwrap()
        .send()
        .await
        .unwrap();
    println!("{}", response.status_code);
    println!("{:?}", response.headers);
    std::io::stdout().write_all(&response.body).unwrap();
}

```
