use talap::Request;
#[tokio::main]
async fn main() {
    use std::io::Write;
    let response = Request::get_from_url("https://www.apple.com/")
        .await
        .unwrap()
        .send()
        .await
        .unwrap();
    println!("{}", response.status_code);
    println!("{:?}", response.headers);
    std::io::stdout().write_all(&response.body).unwrap();
}
